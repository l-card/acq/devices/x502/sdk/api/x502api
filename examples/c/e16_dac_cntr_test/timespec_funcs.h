#ifndef __TIMESPEC_FUNCS_H__
#define __TIMESPEC_FUNCS_H__


#ifdef _WIN32

#define CLOCK_MONOTONIC 1
int clock_gettime(int a, struct timespec *spec);

#endif //_WIN32

void timespec_diff(struct timespec *a, struct timespec *b, struct timespec *result);
double timespec_to_double(struct timespec *ts);

#endif //__TIMESPEC_FUNCS_H__
