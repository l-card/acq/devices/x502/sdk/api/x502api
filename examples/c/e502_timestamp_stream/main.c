/*    
   Пример работы с модулями E502-P1 поддерживающими механизм синхронизации времени по протоколу PTP.
   
   Данный пример представляет из себя консольную программу на языке C,
   демонстрирующую работу синхронного
   ввода данных с АЦП и цифровых линий, а также работу с потоком данных включающим в себя метки времени.

   Перед сбором в примере идет поиск модулей, подключенных по интерфейсам  USB,
   и предоставляется список для выбора модуля, с которым нужно работать.
   Для подключения по Ethernet нужно указать IP-адреса интересующих модулей
   в качестве аргументов командной строки при вызове примера, тогда эти адреса
   будут добавлены в конец списка выбора модуля.
   Например, если интересуют модули с адресами 192.168.1.5 и 192.168.1.6,
   то пример можно вызвать:
   x502_stream_read 192.168.1.5 192.168.1.6
   и две дополнительные строки с этими адресами появятся в списке выбора.

   Настройки частот, количества принимаемых данных и т.д. задаются с помощью макросов в
   начале программы.
   Настройки логических каналов - с помощью таблиц f_channels/f_ch_modes/f_ch_ranges.

   Пример выполняет прием блоков данных заданного размера.
   Сбор идет до нажатия любой клавиши на Windows или  CTRL+C на Linux.

   Пример также показывает как выполнять обработку данных и определять начало кадра,
   в случае если в X502_ProcessData() передается не целое число кадров.

   Данный пример содержит проект для Visual Studio 2008, а также может быть собран
   gcc в Linux или mingw в Windows через makefile или с помощью cmake (подробнее
   в комментариях в соответствующих файлах).

   Для того чтобы собрать проект в Visual Studio, измените путь к заголовочным файлам
   (Проект (Project) -> Свойства (Properties) -> Свойства конфигурации (Configuration Properties)
   -> С/С++ -> Общие (General) -> Дополнительные каталоги включения (Additional Include Directories))
   на тот, где у вас лежат заголовочный файлы x502api.h, l502api.h и e502api.h и измените путь к библиотекам
   (Проект (Project) -> Свойства (Properties) -> Свойства конфигурации (Configuration Properties) ->
   Компановщик (Linker) -> Общие (General) -> Дополнительные катологи библиотек (Additional Library Directories)).

   Внимание!!: Если Вы собираете проект под Visual Studio и взяли проект с сайта (а не из SDK),
   то для корректного отображения русских букв в программе нужно изменить кодировку
   или указать сохранение с сигнатурой кодировки для UTF-8:
   выберите Файл (File) -> Дополнительные параметры сохранения (Advanced Save Options)...
   и в поле Кодировка (Encoding) выберите Юникод (UTF8, с сигнатурой)/Unicode (UTF-8 with signature)
   и сохраните изменения в файле.

   */

#include "l502api.h"
#include "e502api.h"
#include "x502tstp.h"

#include "../../devs/e502/e502_fpga_regs.h"

#ifdef _WIN32
#include <locale.h>
#include <conio.h>

#else
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <inttypes.h>

#include "x502api_private.h"

#define STREAM_IN_WRD_TYPE(wrd) wrd & 0x80000000 ? STREAM_IN_WRD_ADC : \
      (wrd & 0xFF000000) == 0x0 ? STREAM_IN_WRD_DIN : \
      (wrd & 0xF0000000) == 0x40000000 ? STREAM_IN_WRD_TSP : \
    ((wrd & 0xFF000000)>>24) == 0x01 ? STREAM_IN_WRD_MSG : STREAM_IN_WRD_USR

#ifdef _WIN32

#define CLOCK_MONOTONIC 1

int clock_gettime(int a, struct timespec *spec) {
    __int64 wintime; GetSystemTimeAsFileTime((FILETIME*)&wintime);
    wintime      -=116444736000000000i64;  //1jan1601 to 1jan1970
    spec->tv_sec  =wintime / 10000000i64;           //seconds
    spec->tv_nsec =wintime % 10000000i64 *100;      //nano-seconds
    return 0;
}
#endif    

#define NSEC_PER_SEC 1000000000

void timespec_diff(struct timespec *a, struct timespec *b,
    struct timespec *result) {
    result->tv_sec  = a->tv_sec  - b->tv_sec;
    result->tv_nsec = a->tv_nsec - b->tv_nsec;
    if (result->tv_nsec < 0) {
        --result->tv_sec;
        result->tv_nsec += NSEC_PER_SEC;
    }
}

double timespec_to_double(struct timespec *ts)
{
	return ((double)(ts->tv_sec) + ((double)(ts->tv_nsec) / NSEC_PER_SEC));
}


/* признак необходимости завершить сбор данных */
static int f_out = 0;

/* количество используемых логических каналов */
#define ADC_LCH_CNT  1

/* частота сбора АЦП в Гц*/
#define ADC_FREQ          1000000
#define ADC_FRAME_FREQ    (ADC_FREQ/ADC_LCH_CNT)
/* частота синхронного ввода в Гц*/
#define DIN_FREQ          1000000

#define ENABLE_ADC
#define ENABLE_DIN

// при ADC_FREQ==2000000 и DIN_FREQ==1000000 через Ethernet обязательно будет X502_STREAM_IN_MSG_OVERFLOW


#define TCP_CONNECTION_TOUT 5000


/* сколько отсчетов считываем за блок */
#define READ_BLOCK_SIZE   4096*66*3
/* таймаут на прием блока (мс) */
#define READ_TIMEOUT     2000


/* номера используемых физических каналов */
static uint32_t f_channels[ADC_LCH_CNT] = {0};
/* режимы измерения для каналов */
static uint32_t f_ch_modes[ADC_LCH_CNT] = {X502_LCH_MODE_DIFF};
/* диапазоны измерения для каналов */
static uint32_t f_ch_ranges[ADC_LCH_CNT] = {X502_ADC_RANGE_10};


t_x502_hnd g_hnd = NULL;

#ifndef _WIN32
/* Обработчик сигнала завершения для Linux */
static void f_abort_handler(int sig) {
    f_out = 1;
}
#endif


/* Функция находит все подключенные модули по интерфейсам PCI-Express и USB и
 * сохраняет записи о этих устройствах в выделенный массив.
 * Также создаются записи по переданным IP-адресам модулей и добавляются в конец
 * массива.
 * Указатель на выделенный массив, который должен быть потом очищен, сохраняется
 * в pdevrec_list, а количество действительных элементов (память которых должна
 * быть в дальнейшем освобождена с помощью X502_FreeDevRecordList()) возвращается
 * как результат функции */
static uint32_t f_get_all_devrec(t_x502_devrec **pdevrec_list, uint32_t *ip_addr_list, unsigned ip_cnt) {
    int32_t fnd_devcnt = 0;
    uint32_t pci_devcnt = 0;
    uint32_t usb_devcnt = 0;

    t_x502_devrec *rec_list = NULL;

    /* получаем количество подключенных устройств по интерфейсам PCI и USB */
    //L502_GetDevRecordsList(NULL, 0, 0, &pci_devcnt);
    E502_UsbGetDevRecordsList(NULL, 0, 0, &usb_devcnt);

    if ((pci_devcnt + usb_devcnt + ip_cnt) != 0) {
        /* выделяем память для массива для сохранения найденного количества записей */
        rec_list = malloc((pci_devcnt + usb_devcnt + ip_cnt) * sizeof(t_x502_devrec));

        if (rec_list != NULL) {
            unsigned i;
            /* получаем записи о модулях L502, но не больше pci_devcnt */
            if (pci_devcnt!=0) {
                int32_t res = L502_GetDevRecordsList(&rec_list[fnd_devcnt], pci_devcnt, 0, NULL);
                if (res >= 0) {
                    fnd_devcnt += res;
                }
            }
            /* добавляем записи о модулях E502, подключенных по USB, в конец массива */
            if (usb_devcnt!=0) {
                int32_t res = E502_UsbGetDevRecordsList(&rec_list[fnd_devcnt], usb_devcnt, 0, NULL);
                if (res >= 0) {
                    fnd_devcnt += res;
                }
            }

            /* создаем записи для переданного массива ip-адресов */
            for (i=0; i < ip_cnt; i++) {
                if (E502_MakeDevRecordByIpAddr(&rec_list[fnd_devcnt], ip_addr_list[i],0, TCP_CONNECTION_TOUT) == X502_ERR_OK) {
                    fnd_devcnt++;
                }
            }
        }
    }

    if (fnd_devcnt != 0) {
        /* если создана хотя бы одна запись, то сохраняем указатель на выделенный массив */
        *pdevrec_list = rec_list;
    } else {
        *pdevrec_list = NULL;
        free(rec_list);
    }

    return fnd_devcnt;
}


static t_x502_hnd f_dev_select_open(int argc, char** argv) {
    uint32_t fnd_devcnt,i, dev_ind;
    t_x502_devrec *devrec_list = NULL;
    uint32_t *ip_addr_list = NULL;
    uint32_t ip_cnt = 0;
    t_x502_hnd hnd = NULL;

    /* если есть аргументы командной строки, то предполагаем, что это могут быть
       ip-адреса интересующих устройств. */
    if (argc > 1) {
        ip_addr_list = malloc((argc-1) * sizeof(ip_addr_list[0]));
        if (ip_addr_list == NULL) {
            fprintf(stderr, "Ошибка выделения памяти!\n");
        } else {
            for (i=1; (int)i < argc; i++) {
                int a[4];
                if (sscanf(argv[i], "%d.%d.%d.%d", &a[0], &a[1], &a[2], &a[3])==4) {
                    ip_addr_list[ip_cnt++] = ((a[0] & 0xFF) << 24) |
                                             ((a[1] & 0xFF) << 16) |
                                             ((a[2] & 0xFF) <<  8) |
                                             (a[3] & 0xFF);
                }
            }
        }
    }

    /* получаем список модулей для выбора */
    fnd_devcnt = f_get_all_devrec(&devrec_list, ip_addr_list, ip_cnt);

    if (fnd_devcnt == 0) {
        printf("Не найдено ни одного модуля\n");
    } else {
        /* выводим информацию по списку модулей */
        printf("Доступны следующие модули:\n");
        for (i=0; i < fnd_devcnt; i++) {
            printf("Модуль № %d: %s, %-9s", i, devrec_list[i].devname,
                   devrec_list[i].iface == X502_IFACE_PCI ? "PCI/PCIe" :
                   devrec_list[i].iface == X502_IFACE_USB ? "USB" :
                   devrec_list[i].iface == X502_IFACE_ETH ? "Ethernet" : "?");

            /* при подключении по сети по IP-адресу серийный номер можно узнать
               только после открытия соединения. При этом поле location
               содержит строку с описанием адреса устройства */
            if (devrec_list[i].iface != X502_IFACE_ETH) {
                printf("Сер. номер: %s\n", devrec_list[i].serial);
            } else {
                printf("Адрес: %s\n", devrec_list[i].location);
            }
        }

        /* выбираем нужный по введенному номеру модуля по порядку с клавиатуры */
        printf("Введите номер модуля, с которым хотите работать (от 0 до %d)\n", fnd_devcnt-1);
        fflush(stdout);
        scanf("%d", &dev_ind);

        if (dev_ind >= fnd_devcnt) {
            printf("Неверно указан номер модуля...\n");
        } else {
            /* если ввели номер правильно - создаем описатель */
            hnd = X502_Create();
            if (hnd==NULL) {
                fprintf(stderr, "Ошибка создания описателя модуля!");
            } else {
                /* устанавливаем связь с модулем по записи */
                int32_t err = X502_OpenByDevRecord(hnd, &devrec_list[dev_ind]);
                if (err != X502_ERR_OK) {
                    fprintf(stderr, "Ошибка установления связи с модулем: %s!", X502_GetErrorString(err));
                    X502_Free(hnd);
                    hnd = NULL;
                }
            }
        }

        /* освобождение ресурсов действительных записей из списка */
        X502_FreeDevRecordList(devrec_list, fnd_devcnt);
        /* очистка памяти самого массива */
        free(devrec_list);
    }

    /* освобождаем выделенный массив под IP-адреса (если был выделен) */
    free(ip_addr_list);

    return hnd;
}

/* настройка параметров модуля */
int32_t f_setup_params(t_x502_hnd hnd) {
    int32_t err = X502_ERR_OK, i;

    /* устанавливаем параметры логической таблицы АЦП */
    err = X502_SetLChannelCount(hnd, ADC_LCH_CNT);
    for (i=0; (i < ADC_LCH_CNT) && (err == X502_ERR_OK); i++)
        err = X502_SetLChannel(hnd, i, f_channels[i], f_ch_modes[i], f_ch_ranges[i], 0);

    /* устанавливаем частоты ввода для АЦП и цифровых входов */
    if (err == X502_ERR_OK) {
        double f_adc = ADC_FREQ, f_frame = ADC_FRAME_FREQ, f_din = DIN_FREQ;

        err = X502_SetAdcFreq(hnd, &f_adc, &f_frame);
        if (err == X502_ERR_OK)
            err = X502_SetDinFreq(hnd, &f_din);
        if (err == X502_ERR_OK) {
            /* выводим реально установленные значения - те что вернули функции */
            printf("Установлены частоты:\n    Частота сбора АЦП = %0.0f\n"
                "    Частота на лог. канал = %0.0f\n    Частота цифрового ввода = %0.0f\n",
                f_adc, f_frame, f_din);
        }
    }

    /* записываем настройки в модуль */
    if (err == X502_ERR_OK)
        err = X502_Configure(hnd, 0);

    /* разрешаем синхронные потоки */
    if (err == X502_ERR_OK) {
        int streams = 0;
#ifdef ENABLE_ADC
        streams |= X502_STREAM_ADC;
#endif
#ifdef ENABLE_DIN
        streams |= X502_STREAM_DIN;
#endif
        err = X502_StreamsEnable(hnd, streams);        
    }
    return err;
}

int main(int argc, char** argv) {
    int32_t err = X502_ERR_OK;
    uint32_t ver;
#ifndef _WIN32
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    /* В ОС Linux устанавливаем свой обработчик на сигнал закрытия,
       чтобы завершить сбор корректно */
    sa.sa_handler = f_abort_handler;
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
#endif
#ifdef _WIN32
    /* для вывода русских букв в консоль для ОС Windows в CP1251 без перевода в OEM */
    setlocale(LC_CTYPE, "");
#endif
    /* получаем версию библиотеки */
    ver = X502_GetLibraryVersion();
    printf("Версия библиотеки: %d.%d.%d\n", (ver >> 24)&0xFF, (ver>>16)&0xFF, (ver>>8)&0xFF);

    /********** Получение списка устройств и выбор, с каким будем работать ******************/
    g_hnd = f_dev_select_open(argc, argv);

    /********************************** Работа с модулем **************************/
    /* если успешно выбрали модуль и установили с ним связь - продолжаем работу */
    if (g_hnd != NULL) {
        /* получаем информацию */
        t_x502_info info;
        err = X502_GetDevInfo(g_hnd, &info);
        if (err != X502_ERR_OK) {
            fprintf(stderr, "Ошибка получения серийного информации о модуле: %s!", X502_GetErrorString(err));
        } else {
            /* выводим полученную информацию */
            printf("Установлена связь со следующим модулем:\n");
            printf(" Серийный номер          : %s\n", info.serial);
            printf(" Наличие ЦАП             : %s\n", info.devflags & X502_DEVFLAGS_DAC_PRESENT ? "Да" : "Нет");
            printf(" Наличие BlackFin        : %s\n", info.devflags & X502_DEVFLAGS_BF_PRESENT ? "Да" : "Нет");
            printf(" Наличие гальваноразвязки: %s\n", info.devflags & X502_DEVFLAGS_GAL_PRESENT ? "Да" : "Нет");
            printf(" Индустриальное исп.     : %s\n", info.devflags & X502_DEVFLAGS_INDUSTRIAL ? "Да" : "Нет");
            printf(" Наличие интерф. PCI/PCIe: %s\n", info.devflags & X502_DEVFLAGS_IFACE_SUPPORT_PCI ? "Да" : "Нет");
            printf(" Наличие интерф. USB     : %s\n", info.devflags & X502_DEVFLAGS_IFACE_SUPPORT_USB ? "Да" : "Нет");
            printf(" Наличие интерф. Ethernet: %s\n", info.devflags & X502_DEVFLAGS_IFACE_SUPPORT_ETH ? "Да" : "Нет");
            printf(" Версия ПЛИС             : %d.%d\n", (info.fpga_ver >> 8) & 0xFF, info.fpga_ver & 0xFF);
            printf(" Версия PLDA             : %d\n", info.plda_ver);
            if (info.mcu_firmware_ver != 0) {
                printf(" Версия прошивки ARM     : %d.%d.%d.%d\n",
                       (info.mcu_firmware_ver >> 24) & 0xFF,
                       (info.mcu_firmware_ver >> 16) & 0xFF,
                       (info.mcu_firmware_ver >>  8) & 0xFF,
                       info.mcu_firmware_ver & 0xFF);
            }
        }

        if (err == X502_ERR_OK) {
            /* настраиваем параметры модуля */
            err = f_setup_params(g_hnd);
            if (err != X502_ERR_OK)
                fprintf(stderr, "Ошибка настройки модуля: %s!", X502_GetErrorString(err));
        }

        /*err = X502_FpgaRegRead(g_hnd, E502_REGS_ARM_BLOCK, &val);
        if (err) {
            printf("X502_FpgaRegRead err=%d\n", err);
        } else {
            printf("X502_FpgaRegRead(E502_REGS_ARM_BLOCK) = %d\n", val);
        } */
        
        // Включаем метки времени в потоке
        X502_FpgaRegWrite(g_hnd, E502_REGS_ARM_TIME_CTRL, 1); 

        X502_FpgaRegWrite(g_hnd, E502_REGS_IOHARD_GO_SYNC_IO, 0);
        X502_FpgaRegWrite(g_hnd, E502_REGS_BF_CTL, X502_REGBIT_BF_CTL_BF_RESET_Msk);

        X502_SetSyncMode(g_hnd, X502_SYNC_INTERNAL);

        /* запуск синхронного ввода-вывода */
        if (err == X502_ERR_OK) {
            err = X502_StreamsStart(g_hnd);
            if (err != X502_ERR_OK) {
                fprintf(stderr, "Ошибка запуска сбора данных: %s!\n", X502_GetErrorString(err));
            }
        }

        if (err == X502_ERR_OK) {
            int block;
            int32_t stop_err;

            printf("Сбор данных запущен. Для останова нажмите %s\n",
#ifdef _WIN32
                   "любую клавишу"
#else
                   "CTRL+C"
#endif
                   );
            fflush(stdout);

            struct timespec start_time;
            
            clock_gettime(CLOCK_MONOTONIC, &start_time);

            int32_t g_rcv_cnt = 0;
            int cntr = 0;
            int rcv_cntr = 0;

            t_x502_tstp_state tstp_state;

            X502_tstp_init(&tstp_state, ADC_FREQ, DIN_FREQ);


            for (block = 0; (err == X502_ERR_OK) && !f_out; block++) {
                int32_t rcv_size;
                uint32_t adc_size, din_size;
                uint32_t adc_wrd_num = 0;
                uint32_t din_wrd_num = 0;

                /* массив для приема необработанных данных */
                static uint32_t rcv_buf[READ_BLOCK_SIZE];
                static uint32_t snd_buf[READ_BLOCK_SIZE];
                static double   adc_data[READ_BLOCK_SIZE];
                static uint32_t din_data[READ_BLOCK_SIZE];
                
                int i;

                struct timespec cur_time;
                clock_gettime(CLOCK_MONOTONIC, &cur_time);

                /* принимаем данные (по таймауту) */
                rcv_size = X502_Recv(g_hnd, rcv_buf, READ_BLOCK_SIZE, READ_TIMEOUT);
                if (rcv_size < 0) {
                    err = rcv_size;
                    fprintf(stderr, "Ошибка приема данных: %s\n", X502_GetErrorString(err));
                    continue;
                }
                adc_size = sizeof(adc_data)/sizeof(adc_data[0]);
                din_size = sizeof(din_data)/sizeof(din_data[0]);

                /* обрабатываем принятые данные, распределяя их на данные АЦП и цифровых входов */
                err = X502_ProcessData(g_hnd, rcv_buf, rcv_size, X502_PROC_FLAGS_VOLT,
                                        adc_data, &adc_size, din_data, &din_size);
                if (err != X502_ERR_OK) {
                    fprintf(stderr, "Ошибка обработки данных: %s\n", X502_GetErrorString(err));
                    continue;
                }

                g_rcv_cnt += rcv_size;
                int error = 0;
                
                clock_gettime(CLOCK_MONOTONIC, &cur_time);

                if (cur_time.tv_sec  - start_time.tv_sec >= 5) {
                    //printf("rcv speed=%lld wrds/sec cntr=%x\n", (g_rcv_cnt) / (cur_time.tv_sec  - start_time.tv_sec), rcv_cntr);
                    start_time.tv_sec = cur_time.tv_sec;
                    g_rcv_cnt = 0;
                }
                
                for (i = 0; i < rcv_size; i++) {
                    uint32_t cur_wrd = rcv_buf[i];
                    uint64_t cur_wrd_time;
                    t_stream_in_wrd_type type = STREAM_IN_WRD_TYPE(cur_wrd);
                    
                    X502_tstp_process_wrd(&tstp_state, cur_wrd);
                    X502_tstp_get_curwrd_time(&tstp_state, &cur_wrd_time) ;

                    /* проверяем - это данные от АЦП или цифровых входов */
                    if (type == STREAM_IN_WRD_DIN) {
                            uint16_t din_wrd;
                            static bool last_din_wrd_initialized = false;
                            static uint16_t last_din_wrd;

                            if (!tstp_state.tstp_mark_rcvd) {
                                fprintf(stderr, "No timestamp received, DIN wrd unexpected!\n");
                                break;
                            }

                            din_wrd = din_data[din_wrd_num];
                            din_wrd_num++;
                            
                            #define DIN_PPS_MASK    1

                            if (!last_din_wrd_initialized) {
                                last_din_wrd = din_wrd;
                                last_din_wrd_initialized = true;
                            } else
                            if ((din_wrd & DIN_PPS_MASK) != (last_din_wrd & DIN_PPS_MASK) && (din_wrd & DIN_PPS_MASK)) {
                                // дождались положительный фронт сигнала PPS на ножке DI1
                                static bool last_pps_edge_time_initialized = false;
                                static uint64_t last_pps_edge_time = 0;

                                X502_tstp_get_curwrd_time(&tstp_state, &cur_wrd_time);
                                
                                {
                                    static uint32_t nsec_max = 0;
                                    static uint32_t nsec_min = -1;
                                    uint64_t sec_time;
                                    uint64_t ns_time;
                                    sec_time = cur_wrd_time - TSTP_SEC_TO_SSEC(TSTP_SECSSEC_TO_SEC(cur_wrd_time));
                                    ns_time = TSTP_SSEC_TO_NSEC(sec_time);
                                    if (ns_time > 500000000) {
                                        ns_time = NSEC_PER_SEC - ns_time;
                                    }
                                    if (ns_time > nsec_max) {
                                        nsec_max = ns_time;
                                    }
                                    if (ns_time < nsec_min) {
                                        nsec_min = ns_time;
                                    }
                                    printf("DIN PPS positive edge: %ld ns (max - min  = %d)\n", ns_time, nsec_max - nsec_min);
                                }
                                
                                if (last_pps_edge_time_initialized ) {
                                    uint64_t pps_period;
                                    pps_period = cur_wrd_time - last_pps_edge_time;
                                    printf("DIN PPS positive edge pediod = %d.%09d\n", TSTP_SECSSEC_TO_SEC(pps_period), TSTP_SSEC_TO_NSEC(pps_period));
                                } else {
                                    last_pps_edge_time_initialized = true;
                                }

                                last_pps_edge_time = cur_wrd_time;
                            }
                            last_din_wrd = din_wrd;
                    } else
                    if (type == STREAM_IN_WRD_ADC) {
                            static uint16_t last_adc_wrd;
                            register uint32_t wrd = rcv_buf[i];
                            double adc_val;
                            static bool adc_wait_for_edge = false;

                            if (!tstp_state.tstp_mark_rcvd) {
                                fprintf(stderr, "No timestamp received, ADC wrd unexpected!\n");
                                break;
                            }

                            adc_val = adc_data[adc_wrd_num];
                            adc_wrd_num++;

                            X502_tstp_get_curwrd_time(&tstp_state, &cur_wrd_time);
                            
                            if (adc_wait_for_edge) {
                                if (adc_val > 2.4) {
                                    // дождались положительный фронт сигнала PPS на 1 канале ADC
                                    static bool last_pps_edge_time_initialized = false;
                                    static uint64_t last_pps_edge_time = 0;

                                    adc_wait_for_edge = false;
                                    
                                    {
                                        static uint32_t nsec_max = 0;
                                        static uint32_t nsec_min = -1;
                                        uint64_t sec_time;
                                        uint64_t ns_time;
                                        sec_time = cur_wrd_time - TSTP_SEC_TO_SSEC(TSTP_SECSSEC_TO_SEC(cur_wrd_time));
                                        ns_time = TSTP_SSEC_TO_NSEC(sec_time);
                                        if (ns_time > 500000000) {
                                            ns_time = NSEC_PER_SEC - ns_time;
                                        }
                                        if (ns_time > nsec_max) {
                                            nsec_max = ns_time;
                                        }
                                        if (ns_time < nsec_min) {
                                            nsec_min = ns_time;
                                        }
                                        printf("ADC PPS positive edge: %ld ns (max - min  = %d)\n", ns_time, nsec_max - nsec_min);
                                    }
                                    
                                    if (last_pps_edge_time_initialized ) {
                                        uint64_t pps_period;
                                        pps_period = cur_wrd_time - last_pps_edge_time;
                                        
                                        printf("ADC PPS positive edge period = %d.%09d\n", TSTP_SECSSEC_TO_SEC(pps_period),
                                            TSTP_SSEC_TO_NSEC(pps_period));

                                    } else {
                                        last_pps_edge_time_initialized = true;
                                    }

                                    last_pps_edge_time = cur_wrd_time;
                                }
                            } else
                            if (adc_val < 2.3) {
                                adc_wait_for_edge = true;
                            }
                    }

                    rcv_cntr++;
                }
#if 0
                adc_size = sizeof(adc_data)/sizeof(adc_data[0]);
                din_size = sizeof(din_data)/sizeof(din_data[0]);
                int adc_data_num = 0;

                /* обрабатываем принятые данные, распределяя их на данные АЦП и цифровых входов */
                err = X502_ProcessData(g_hnd, rcv_buf, rcv_size, X502_PROC_FLAGS_VOLT,
                                        adc_data, &adc_size, din_data, &din_size);
                if (err != X502_ERR_OK) {
                    fprintf(stderr, "Ошибка обработки данных: %s\n", X502_GetErrorString(err));
                    continue;
                }
#endif
                
                if ((tstp_state.dinwrds_after_tstp / DIN_FREQ) > 2) 
                {
                    printf("Warning: timestamp mark undetected after %d seconds\n", tstp_state.dinwrds_after_tstp / DIN_FREQ);
                }
                if ((tstp_state.adcwrds_after_tstp / ADC_FREQ) > 2) 
                {
                    printf("Warning: timestamp mark undetected after %d seconds\n", tstp_state.adcwrds_after_tstp / ADC_FREQ);
                }

                continue;

#ifdef _WIN32
                /* проверка нажатия клавиши для выхода */
                if (_kbhit())
                    f_out = 1;
#endif
            }
            
            /* останавливаем поток сбора данных (независимо от того, была ли ошибка) */
            stop_err = X502_StreamsStop(g_hnd);
            if (stop_err != X502_ERR_OK) {
                fprintf(stderr, "Ошибка останова сбора данных: %s\n", X502_GetErrorString(err));
                if (err == X502_ERR_OK)
                    err = stop_err;
            } else {
                printf("Сбор данных остановлен успешно\n");
            }

            // Выключаем метки времени в потоке
            X502_FpgaRegWrite(g_hnd, E502_REGS_ARM_TIME_CTRL, 0);
        }

        /* закрываем связь с модулем */
        X502_Close(g_hnd);
        /* освобождаем описатель */
        X502_Free(g_hnd);
    }
    return err;
}
